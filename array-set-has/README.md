Testing how fast it is to find an item in a Set vs an Array

```
$ deno run -A array-set-has/bench.mjs 
cpu: AMD Ryzen 7 5700U with Radeon Graphics
runtime: deno 1.26.0 (x86_64-unknown-linux-gnu)

benchmark           time (avg)             (min … max)       p75       p99      p995
------------------------------------------------------ -----------------------------
Set#has         425.92 ps/iter     (363 ps … 25.79 ns)    419 ps  782.3 ps    1.2 ns
Array#includes     4.4 ns/iter    (3.91 ns … 38.58 ns)   4.16 ns   9.78 ns  11.65 ns
Array#indexOf    79.27 ns/iter   (76.38 ns … 89.09 ns)  79.87 ns  81.77 ns  82.16 ns
```
