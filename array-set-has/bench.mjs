import { run, bench, group } from 'https://esm.sh/mitata';


const array = Array.from({ length: 10e3 }, (val, idx) => idx);
const set = new Set(array);


bench('Set#has', () => {
	set.has(1234);
});

bench('Array#includes', () => {
	array.includes(1234);
});

bench('Array#indexOf', () => {
	array.indexOf(1234) !== -1;
});


await run();
