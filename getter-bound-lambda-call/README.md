Tests the performance of calling a bound function vs. calling an arrow function
vs. invoking a getter and setter, meant for a reactivity system.

```
$ deno run -A getter-bound-lambda-call/bench.mjs 
cpu: AMD Ryzen 7 5700U with Radeon Graphics
runtime: deno 1.26.0 (x86_64-unknown-linux-gnu)

benchmark           time (avg)             (min … max)       p75       p99      p995
------------------------------------------------------ -----------------------------
getter/setter   633.48 ps/iter   (474.9 ps … 20.76 ns)  586.7 ps    1.7 ns    4.8 ns
bound function    4.45 ns/iter    (3.97 ns … 35.06 ns)   4.22 ns   9.47 ns  10.62 ns
arrow function   10.25 ns/iter     (8.6 ns … 62.07 ns)   9.22 ns   32.1 ns  37.02 ns
```
