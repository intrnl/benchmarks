import { run, bench, group } from 'https://esm.sh/mitata';


class RefGetter {
	constructor (value) {
		this._value = value;
	}

	get value () {
		return this._value;
	}
	set value (next) {
		this._value = next;
	}
}

const access = Symbol();

class RefBoundCall {
	constructor (value) {
		this._value = value;
	}

	run (next) {
		if (next === access) {
			return this._value;
		}
		else {
			this._value = next;
			return next;
		}
	}
}

function refGetter (value) {
	const instance = new RefGetter(value);

	return instance;
}

function refBoundCall (value) {
	const instance = new RefBoundCall(value);
	const bound = instance.run.bind(instance);

	return bound;
}

function refLambda (value) {
	return function ref (next) {
		if (next === access) {
			return value;
		}
		else {
			value = next;
			return next;
		}
	}
}


const getter = refGetter(0);
const boundCall = refBoundCall(0);
const lambda = refLambda(0);

bench('getter/setter', () => {
	getter.value = 1;

	getter.value += 2;
	getter.value *= 3;
	getter.value /= 4;
});

bench('bound function', () => {
	boundCall(1);

	boundCall(boundCall(access) + 2);
	boundCall(boundCall(access) * 3);
	boundCall(boundCall(access) / 4);
});

bench('arrow function', () => {
	lambda(1);

	lambda(lambda(access) + 2);
	lambda(lambda(access) * 3);
	lambda(lambda(access) / 4);
});

await run();
