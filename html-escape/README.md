Tests various HTML escaping functions

```
$ deno run -A html-escape/bench.mjs 
cpu: AMD Ryzen 7 5700U with Radeon Graphics
runtime: deno 1.26.0 (x86_64-unknown-linux-gnu)

benchmark               time (avg)             (min … max)       p75       p99      p995
---------------------------------------------------------- -----------------------------
• no escaping - 10 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   136.46 ns/iter (122.33 ns … 292.26 ns) 138.81 ns  181.8 ns 193.54 ns
escape_re_exec       25.68 ns/iter   (24.16 ns … 77.72 ns)  25.78 ns  35.25 ns  38.61 ns
escape_re_exec_alt   25.41 ns/iter    (23.86 ns … 75.2 ns)  26.01 ns  34.75 ns  38.16 ns
escape_no_re          23.4 ns/iter   (22.88 ns … 29.36 ns)  23.69 ns  25.95 ns  26.54 ns

summary for no escaping - 10 chars
  escape_no_re
   1.09x faster than escape_re_exec_alt
   1.1x faster than escape_re_exec
   5.83x faster than escape_re_replace

• no escaping - 20 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   146.91 ns/iter (132.81 ns … 272.57 ns) 149.65 ns 170.21 ns 265.41 ns
escape_re_exec       31.54 ns/iter    (29.78 ns … 76.6 ns)  31.43 ns     49 ns  60.84 ns
escape_re_exec_alt   46.98 ns/iter  (28.75 ns … 106.57 ns)  47.21 ns  51.18 ns  58.58 ns
escape_no_re         47.19 ns/iter   (45.73 ns … 73.58 ns)  47.02 ns  63.16 ns  66.85 ns

summary for no escaping - 20 chars
  escape_re_exec
   1.49x faster than escape_re_exec_alt
   1.5x faster than escape_no_re
   4.66x faster than escape_re_replace

• no escaping - 50 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   160.35 ns/iter  (145.4 ns … 265.83 ns) 163.17 ns 210.21 ns 217.45 ns
escape_re_exec       53.67 ns/iter   (44.95 ns … 79.14 ns)  52.88 ns  77.41 ns  77.69 ns
escape_re_exec_alt   62.59 ns/iter  (44.19 ns … 124.17 ns)  62.97 ns   65.2 ns  66.49 ns
escape_no_re        103.04 ns/iter (101.77 ns … 144.93 ns) 103.25 ns 106.43 ns 110.09 ns

summary for no escaping - 50 chars
  escape_re_exec
   1.17x faster than escape_re_exec_alt
   1.92x faster than escape_no_re
   2.99x faster than escape_re_replace

• no escaping - 100 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   188.36 ns/iter (173.09 ns … 279.66 ns) 192.45 ns 219.35 ns 274.63 ns
escape_re_exec       81.27 ns/iter  (78.44 ns … 208.85 ns)  81.63 ns  89.76 ns  95.18 ns
escape_re_exec_alt   91.05 ns/iter   (81.6 ns … 108.78 ns)  91.38 ns  98.84 ns 105.04 ns
escape_no_re        197.62 ns/iter (194.29 ns … 222.42 ns) 197.64 ns 211.61 ns    221 ns

summary for no escaping - 100 chars
  escape_re_exec
   1.12x faster than escape_re_exec_alt
   2.32x faster than escape_re_replace
   2.43x faster than escape_no_re

• need escaping - 10 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   419.21 ns/iter (408.56 ns … 477.05 ns) 422.52 ns 450.18 ns 477.05 ns
escape_re_exec      188.76 ns/iter (179.12 ns … 241.45 ns) 194.18 ns 220.77 ns 222.25 ns
escape_re_exec_alt  168.56 ns/iter (163.23 ns … 199.57 ns) 169.85 ns 181.64 ns 184.12 ns
escape_no_re        109.32 ns/iter  (96.49 ns … 174.99 ns) 112.78 ns 123.64 ns 129.79 ns

summary for need escaping - 10 chars
  escape_no_re
   1.54x faster than escape_re_exec_alt
   1.73x faster than escape_re_exec
   3.83x faster than escape_re_replace

• need escaping - 20 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   384.04 ns/iter (374.92 ns … 430.85 ns) 385.54 ns  404.2 ns 430.85 ns
escape_re_exec      126.93 ns/iter (116.27 ns … 183.62 ns) 131.21 ns 143.81 ns 158.79 ns
escape_re_exec_alt   114.2 ns/iter (102.63 ns … 134.82 ns) 118.75 ns 131.46 ns 132.86 ns
escape_no_re        103.17 ns/iter  (94.95 ns … 153.09 ns) 109.79 ns    123 ns 133.11 ns

summary for need escaping - 20 chars
  escape_no_re
   1.11x faster than escape_re_exec_alt
   1.23x faster than escape_re_exec
   3.72x faster than escape_re_replace

• need escaping - 50 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   535.02 ns/iter  (516.28 ns … 596.7 ns) 539.21 ns 590.14 ns  596.7 ns
escape_re_exec      340.24 ns/iter  (330.06 ns … 365.2 ns) 343.58 ns 365.06 ns  365.2 ns
escape_re_exec_alt   325.6 ns/iter  (309.19 ns … 448.2 ns) 327.85 ns 370.84 ns  448.2 ns
escape_no_re        298.65 ns/iter (284.89 ns … 434.96 ns) 303.97 ns 325.92 ns 434.96 ns

summary for need escaping - 50 chars
  escape_no_re
   1.09x faster than escape_re_exec_alt
   1.14x faster than escape_re_exec
   1.79x faster than escape_re_replace

• need escaping - 100 chars
---------------------------------------------------------- -----------------------------
escape_re_replace   566.95 ns/iter (538.54 ns … 679.67 ns) 570.92 ns 679.67 ns 679.67 ns
escape_re_exec      323.71 ns/iter (313.69 ns … 353.91 ns) 326.43 ns 350.87 ns 353.91 ns
escape_re_exec_alt  437.91 ns/iter (419.59 ns … 604.86 ns) 441.05 ns 479.46 ns 604.86 ns
escape_no_re        404.89 ns/iter (392.35 ns … 441.05 ns)  410.9 ns 429.26 ns 441.05 ns

summary for need escaping - 100 chars
  escape_re_exec
   1.25x faster than escape_no_re
   1.35x faster than escape_re_exec_alt
   1.75x faster than escape_re_replace
```
