import { run, bench, group } from 'https://esm.sh/mitata';


function escape_re_replace (value) {
	const str = '' + value;
	const res = str.replace(/[&<]/g, (char) => '&#' + char.charCodeAt(0) + ';');

	return res;
}

const ATTR_REGEX = /[&"]/g;
const TEXT_REGEX = /[&<]/g;

function escape_re_exec (value, is_attr = false) {
	const str = '' + value;

	const pattern = is_attr ? ATTR_REGEX : TEXT_REGEX;
	pattern.lastIndex = 0;

	let escaped = '';
	let last = 0;

	while (pattern.exec(str)) {
		const idx = pattern.lastIndex - 1;

		escaped += str.substring(last, idx) + ('&#' + str.charCodeAt(idx) + ';');
		last = idx + 1;
	}

	if (last === 0) {
		return str;
	}

	return escaped + str.substring(last);
}

const ESCAPE_REGEX_ALT = /[&<"]/;

function escape_re_exec_alt (value) {
	const str = '' + value;
	const match = ESCAPE_REGEX_ALT.exec(str);

	if (!match) {
		return str;
	}

	const len = str.length;

	let escaped = '';
	let idx = match.index;
	let last = 0;

	for (; idx < len; idx++) {
		const char = str.charCodeAt(idx);

		switch (char) {
			case 38: case 34: case 60: {
				if (last !== idx) {
					escaped += str.substring(last, idx)
				}

				last = idx + 1
				escaped += ('&#' + char + ';');
			}
		}
	}

  return last !== idx
    ? escaped + str.substring(last, idx)
    : escaped
}

function escape_no_re (value, is_attr = false) {
	const str = '' + value;

	let escaped = '';
	let last = 0;

	for (let idx = 0, len = str.length; idx < len; idx++) {
		const char = str.charCodeAt(idx);

		if (char === 38 || char === (is_attr ? 34 : 60)) {
			escaped += str.substring(last, idx) + ('&#' + char + ';');
			last = idx + 1;
		}
	}

	if (last === 0) {
		return str;
	}

	return escaped + str.substring(last);
}


group('no escaping - 10 chars', () => {
	const str = '5TXzaokquf';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

group('no escaping - 20 chars', () => {
	const str = 'TfiFuUvK6XuJ2TUjGE6o';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

group('no escaping - 50 chars', () => {
	const str = 'LbyfixabVXyp4BMg9eGXfXz58DDtzmq4g3XCMcox3wnnbhgYkj';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

group('no escaping - 100 chars', () => {
	const str = 'YXWukpd56bdFqcVWH3JstHy6DJpcUNfzB8M837ZofTgTpMwDuq9xN3p567SMXBT7Wo5uxAynv5ocEuoMEqSBFW4xGqBSYHDFur3M';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

group('need escaping - 10 chars', () => {
	const str = '<uGvAb.&z9';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

group('need escaping - 20 chars', () => {
	const str = 'm@+nhM2(vQ*mvFtGT8&u';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});


group('need escaping - 50 chars', () => {
	const str = 'okLoHV&Jr+DxWo%uj&p%-VHh@-+)FzfU99db$<j9fz49PSm7En';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

group('need escaping - 100 chars', () => {
	const str = 'p^jiTg2<4.c.FR5@iY!aed#BaHCryTr8zuGmC7A3@Y-8+=t@TZB3A<.PdLV_V-VcKX.d^TinBDRHSPt=org7nY#+-k6W@p<!2o3)';

	bench('escape_re_replace', () => {
		escape_re_replace(str, false);
	});

	bench('escape_re_exec', () => {
		escape_re_exec(str, false);
	});

	bench('escape_re_exec_alt', () => {
		escape_re_exec_alt(str, false);
	});

	bench('escape_no_re', () => {
		escape_no_re(str, false);
	});
});

await run();
