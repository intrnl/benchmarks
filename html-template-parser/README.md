Testing the best format to parse a DOM template from

Note that this is measuring performance for server-side, for browsers, HTML
parsing will win as the entirety of the template are passed off to native HTML
parser.

```
$ deno run -A html-template-parser/bench.mjs 
cpu: AMD Ryzen 7 5700U with Radeon Graphics
runtime: deno 1.26.0 (x86_64-unknown-linux-gnu)

benchmark          time (avg)             (min … max)       p75       p99      p995
----------------------------------------------------- -----------------------------
HTML parsing    64.78 µs/iter   (36.04 µs … 18.73 ms)  46.09 µs 116.77 µs 180.47 µs
Builder-style   41.28 µs/iter   (21.79 µs … 15.09 ms)  28.22 µs  53.92 µs  72.63 µs
DOM notation    40.45 µs/iter   (21.51 µs … 15.24 ms)  27.94 µs  41.06 µs  46.93 µs
```
