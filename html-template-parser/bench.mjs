import { run, bench, group } from 'https://esm.sh/mitata';
import { parseHTML, SVGElement } from 'https://esm.sh/linkedom@0.14.12';


const { document } = parseHTML();

// HTML parsing
function template (html) {
	let template = document.createElement('template');
	template.innerHTML = html;

	return template;
}

bench('HTML parsing', () => {
	template("<section class=main> <input id=toggle-all class=toggle-all type=checkbox /> <label for=toggle-all>Mark all as complete</label> <ul class=todo-list> <!> </ul> <footer class=footer> <span class=todo-count> <strong><!></strong> <!> left </span> <ul class=filters> <li><a href=#/>All</a></li> <li><a href=#/active>Active</a></li> <li><a href=#/completed>Completed</a></li> </ul> <!> </footer> </section>");
});

// Builder-style
function html (tag, attrs, children) {
	let node = document.createElement(tag, { is: attrs.is });

	for (let name in attrs) {
		let value = attrs[name];

		if (value === true) {
			node.toggleAttribute(name, value);
		}
		else {
			node.setAttribute(name, value);
		}
	}

	for (let child of children) {
		node.appendChild(child);
	}

	return node;
}

function text (text) {
	return document.createTextNode(text);
}

function marker () {
	return text('');
}

function svg (tag, attrs, children) {
	let node = document.createElementNS(SVG_NS, tag);

	for (let name in attrs) {
		let value = attrs[name];

		if (value === true) {
			node.toggleAttribute(name, value);
		}
		else {
			node.setAttribute(name, value);
		}
	}

	for (let child of children) {
		node.appendChild(child);
	}

	return node;
}

function fragment (children) {
	let fragment = document.createDocumentFragment();

	for (let child of children) {
		fragment.appendChild(child);
	}

	return fragment;
}

bench('Builder-style', () => {
	fragment([
		html('section', { class: 'main' }, [
			text(' '),
			html('input', { id: 'toggle-all', class: 'toggle-all', type: 'checkbox' }, []),
			text(' '),
			html('label', { for: 'toggle-all' }, [
				text('Mark all as complete'),
			]),
			html('ul', { class: 'todo-list' }, [
				text(' '),
				marker(),
				text(' '),
			]),
			html('footer', { class: 'footer' }, [
				html('span', { class: 'todo-count' }, [
					html('strong', {}, [
						marker(),
					]),
					text(' '),
					marker(),
					text(' left '),
				]),
				html('ul', { class: 'filters' }, [
					html('li', {}, [
						html('a', { href: '#/' }, [
							text('All'),
						]),
					]),
					html('li', {}, [
						html('a', { href: '#/active' }, [
							text('Active'),
						]),
					]),
					html('li', {}, [
						html('a', { href: '#/completed' }, [
							text('Completed'),
						]),
					]),
				]),
			]),
		]),
	]);
});

// DOM notation
let ELEMENT_BRANCH = 1;
let ELEMENT_CE_BRANCH = 2; // separate branch for `is`
let ATTRIBUTE_LEAF = 3;
let ATTRIBUTE_BOOLEAN_LEAF = 4;
let TEXT_LEAF = 5;
let WHITESPACE_LEAF = 6; // the same as TEXT_NODE but only one space
let MARKER_LEAF = 7; // the same as TEXT_NODE but no content

function jsdon (array) {
	let fragment = document.createDocumentFragment();
	let curr = fragment;

	let length = array.length;
	let idx = 0;

	while (idx < length) {
		let type = array[idx++];

		switch (type) {
			case ELEMENT_BRANCH: {
				let tag = array[idx++];

				let node = null;

				if (tag === 'svg' || curr instanceof SVGElement) {
					node = document.createElementNS(SVG_NS, tag);
				}
				else {
					node = document.createElement(tag);
				}

				curr.appendChild(node);
				curr = node;
				break;
			}
			case ELEMENT_CE_BRANCH: {
				let tag = array[idx++];
				let is = array[idx++];

				let node = document.createElement(tag, { is });
				node.setAttribute('is', is);

				curr.appendChild(node);
				curr = node;
				break;
			}
			case ATTRIBUTE_LEAF: {
				let name = array[idx++];
				let value = array[idx++];

				curr.setAttribute(name, value);
				break;
			}
			case ATTRIBUTE_BOOLEAN_LEAF: {
				let name = array[idx++];

				curr.toggleAttribute(name, true);
				break;
			}
			case TEXT_LEAF: {
				let text = array[idx++];
				let node = document.createTextNode(text);

				curr.appendChild(node);
				break;
			}
      case WHITESPACE_LEAF: {
        let node = document.createTextNode(' ');

        curr.appendChild(node);
        break;
      }
			case MARKER_LEAF: {
				let node = document.createTextNode('');

				curr.appendChild(node);
				break;
			}
			default: {
				// we're dealing with negative numbers, which means it's time to go up
				while (type < 0) {
					curr = curr.parentNode;
					type++;
				}
			}
		}
	}

	return curr;
}

bench('DOM notation', () => {
	jsdon([
		ELEMENT_BRANCH, 'section',
			ATTRIBUTE_LEAF, 'class', 'main',
			TEXT_LEAF, ' ',
			ELEMENT_BRANCH, 'input',
				ATTRIBUTE_LEAF, 'id', 'toggle-all',
				ATTRIBUTE_LEAF, 'class', 'toggle-all',
				ATTRIBUTE_LEAF, 'type', 'checkbox',
				-1,
			TEXT_LEAF, ' ',
			ELEMENT_BRANCH, 'label',
				ATTRIBUTE_LEAF, 'for', 'toggle-all',
				TEXT_LEAF, 'Mark all as complete',
				-1,
			ELEMENT_BRANCH, 'ul',
				ATTRIBUTE_LEAF, 'class', 'todo-list',
				TEXT_LEAF, ' ',
				MARKER_LEAF,
				TEXT_LEAF, ' ',
				-1,
			ELEMENT_BRANCH, 'footer',
				ATTRIBUTE_LEAF, 'class', 'footer',
				ELEMENT_BRANCH, 'span',
					ATTRIBUTE_LEAF, 'class', 'todo-count',
					ELEMENT_BRANCH, 'strong',
						MARKER_LEAF,
						-1,
					TEXT_LEAF, ' ',
					MARKER_LEAF,
					TEXT_LEAF, ' left ',
					-1,
				ELEMENT_BRANCH, 'ul',
					ATTRIBUTE_LEAF, 'class', 'filters',
					ELEMENT_BRANCH, 'li',
						ELEMENT_BRANCH, 'a',
							ATTRIBUTE_LEAF, 'href', '#/',
							TEXT_LEAF, 'All',
							-2,
					ELEMENT_BRANCH, 'li',
						ELEMENT_BRANCH, 'a',
							ATTRIBUTE_LEAF, 'href', '#/active',
							TEXT_LEAF, 'Active',
							-2,
					ELEMENT_BRANCH, 'li',
						ELEMENT_BRANCH, 'a',
							ATTRIBUTE_LEAF, 'href', '#/completed',
							TEXT_LEAF, 'Completed',
							-5,
	]);
});


await run();
