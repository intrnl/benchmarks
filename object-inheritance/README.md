Testing object inheritance creation

```
$ deno run -A object-inheritance/bench.mjs 
cpu: AMD Ryzen 7 5700U with Radeon Graphics
runtime: deno 1.24.0 (x86_64-unknown-linux-gnu)

benchmark            time (avg)             (min … max)       p75       p99      p995
------------------------------------------------------- -----------------------------
Object.assign     91.86 ms/iter   (91.34 ms … 92.88 ms)  91.98 ms  92.88 ms  92.88 ms
Spread operator  460.13 µs/iter (311.47 µs … 848.09 µs) 527.96 µs 714.84 µs 793.62 µs
Object.create      7.25 ms/iter     (4.8 ms … 18.57 ms)   5.47 ms  18.57 ms  18.57 ms
```
