import { run, bench } from 'https://esm.sh/mitata';


const MAX_ITER = 1000;


bench('Object.assign', () => {
	let obj = {};

	for (let i = 0; i < MAX_ITER; i++) {
		obj = Object.assign({}, obj, { [i]: i });
	}	
});

bench('Spread operator', () => {
	let obj = {};

	for (let i = 0; i < MAX_ITER; i++) {
		obj = { ...obj, [i]: i };
	}	
});

bench('Object.create', () => {
	let obj = {};

	for (let i = 0; i < MAX_ITER; i++) {
		obj = Object.create(obj);
		obj[i] = i;
	}
});


await run();
