Fastest UUIDv4 stringify implementation

```
$ deno run -A uuidv4-stringify/bench.mjs 
cpu: AMD Ryzen 7 5700U with Radeon Graphics
runtime: deno 1.26.0 (x86_64-unknown-linux-gnu)

benchmark                         time (avg)             (min … max)       p75       p99      p995
-------------------------------------------------------------------- -----------------------------
fromCodePoint                 227.06 ns/iter  (216.61 ns … 306.4 ns) 234.88 ns 275.28 ns 301.87 ns
char map                      206.92 ns/iter (194.85 ns … 283.99 ns) 211.59 ns  248.8 ns 255.36 ns
byte to hex                   285.14 ns/iter    (271 ns … 351.54 ns) 289.97 ns 324.42 ns 351.54 ns
byte to hex (no lowercasing)  111.59 ns/iter  (96.43 ns … 165.97 ns) 114.06 ns 137.22 ns 147.72 ns
```
