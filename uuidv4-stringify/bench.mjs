import { run, bench } from 'https://esm.sh/mitata';


const buf = Uint8Array.from([149, 232, 46, 60, 152, 132, 74, 198, 182, 170, 50, 52, 65, 137, 21, 232]);

const CODE = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102];
const CHAR = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

const byteToHex = [];

for (let i = 0; i < 256; ++i) {
	byteToHex.push((i + 0x100).toString(16).slice(1));
}

bench('fromCodePoint', () => {
	return String.fromCodePoint(
		// 8-4-4-4-12
		CODE[buf[0] >>> 4], CODE[buf[0] & 15], CODE[buf[1] >>> 4], CODE[buf[1] & 15],
		CODE[buf[2] >>> 4], CODE[buf[2] & 15], CODE[buf[3] >>> 4], CODE[buf[3] & 15],
		0x2d,
		CODE[buf[4] >>> 4], CODE[buf[4] & 15], CODE[buf[5] >>> 4], CODE[buf[5] & 15],
		0x2d,
		CODE[buf[6] >>> 4], CODE[buf[6] & 15], CODE[buf[7] >>> 4], CODE[buf[7] & 15],
		0x2d,
		CODE[buf[8] >>> 4], CODE[buf[8] & 15], CODE[buf[9] >>> 4], CODE[buf[9] & 15],
		0x2d,
		CODE[buf[10]>>> 4], CODE[buf[10]& 15], CODE[buf[11]>>> 4], CODE[buf[11]& 15],
		CODE[buf[12]>>> 4], CODE[buf[12]& 15], CODE[buf[13]>>> 4], CODE[buf[13]& 15],
		CODE[buf[14]>>> 4], CODE[buf[14]& 15], CODE[buf[15]>>> 4], CODE[buf[15]& 15],
	);
});

bench('char map', () => {
	return (
		// 8-4-4-4-12
		CHAR[buf[0] >>> 4] + CHAR[buf[0] & 15] + CHAR[buf[1] >>> 4] + CHAR[buf[1] & 15] +
		CHAR[buf[2] >>> 4] + CHAR[buf[2] & 15] + CHAR[buf[3] >>> 4] + CHAR[buf[3] & 15] +
		'-' +
		CHAR[buf[4] >>> 4] + CHAR[buf[4] & 15] + CHAR[buf[5] >>> 4] + CHAR[buf[5] & 15] +
		'-' +
		CHAR[buf[6] >>> 4] + CHAR[buf[6] & 15] + CHAR[buf[7] >>> 4] + CHAR[buf[7] & 15] +
		'-' +
		CHAR[buf[8] >>> 4] + CHAR[buf[8] & 15] + CHAR[buf[9] >>> 4] + CHAR[buf[9] & 15] +
		'-' +
		CHAR[buf[10]>>> 4] + CHAR[buf[10]& 15] + CHAR[buf[11]>>> 4] + CHAR[buf[11]& 15] +
		CHAR[buf[12]>>> 4] + CHAR[buf[12]& 15] + CHAR[buf[13]>>> 4] + CHAR[buf[13]& 15] +
		CHAR[buf[14]>>> 4] + CHAR[buf[14]& 15] + CHAR[buf[15]>>> 4] + CHAR[buf[15]& 15]
	);
});

bench('byte to hex', () => {
	// taken from https://github.com/uuidjs/uuid/blob/main/src/stringify.js
	return (
		byteToHex[buf[0]] +
		byteToHex[buf[1]] +
		byteToHex[buf[2]] +
		byteToHex[buf[3]] +
		'-' +
		byteToHex[buf[4]] +
		byteToHex[buf[5]] +
		'-' +
		byteToHex[buf[6]] +
		byteToHex[buf[7]] +
		'-' +
		byteToHex[buf[8]] +
		byteToHex[buf[9]] +
		'-' +
		byteToHex[buf[10]] +
		byteToHex[buf[11]] +
		byteToHex[buf[12]] +
		byteToHex[buf[13]] +
		byteToHex[buf[14]] +
		byteToHex[buf[15]]
	).toLowerCase();
});

bench('byte to hex (no lowercasing)', () => {
	return (
		byteToHex[buf[0]] +
		byteToHex[buf[1]] +
		byteToHex[buf[2]] +
		byteToHex[buf[3]] +
		'-' +
		byteToHex[buf[4]] +
		byteToHex[buf[5]] +
		'-' +
		byteToHex[buf[6]] +
		byteToHex[buf[7]] +
		'-' +
		byteToHex[buf[8]] +
		byteToHex[buf[9]] +
		'-' +
		byteToHex[buf[10]] +
		byteToHex[buf[11]] +
		byteToHex[buf[12]] +
		byteToHex[buf[13]] +
		byteToHex[buf[14]] +
		byteToHex[buf[15]]
	);
});

await run();
